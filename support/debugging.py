import sys
import cadquery as cq


def _ctx_str(self: cq.cq.CQContext):
    return (
        "CQContext:{"
        + f"  pendingWires: {[repr(x) for x in self.pendingWires]}"
        + f", pendingEdges: {[repr(x) for x in self.pendingEdges]}"
        + f", tags: {self.tags}"
        + "}"
    )


def _shape_str(self: cq.occ_impl.shapes.Shape):
    def _fmt_wires(w):
        return f"wire: {[(item.X, item.Y, item.Z) for item in w.Vertices()]}"

    wires = [_fmt_wires(face.outerWire()) for face in self.Faces()]
    # wires = [(item.X, item.Y, item.Z) for sublist in wires for item in sublist]

    return (
        "Shape:{"
        + f" closed: {self.Closed()}"
        + f", bounds: {self.BoundingBox()}"
        + f", center: {self.Center()}"
        + f", valid: {self.isValid()}"
        + f", null: {self.isNull()}"
        + f", type: {self.geomType()}"
        + f", location: {self.location()}"
        # + f", vertices: {len(self.Vertices())} {[(x.X,x.Y,x.Z) for x in self.Vertices()]}"
        + f", faces: {len(self.Faces())} wires {wires}"
        + "}"
    )


def _loc_str(self: cq.occ_impl.geom.Location):
    # self.wrapped.DumpJson(sys.stdout)
    return "Location:{" + f" transform: {self.toTuple()}" + "}"


def _bb_str(self: cq.occ_impl.geom.BoundBox):
    return (
        "BoundBox:{"
        + f"  center: {self.center}"
        + f", xlen: {self.xlen}"
        + f", ylen: {self.ylen}"
        + f", zlen: {self.zlen}"
        + "}"
    )


def _plane_str(self: cq.occ_impl.geom.Plane):
    return (
        "Plane:{"
        + f"  location: {self.location}"
        + f", origin: {self.origin}"
        + f", x_direction: {self.xDir}"
        + f", y_direction: {self.yDir}"
        + f", z_direction: {self.zDir}"
        + "}"
    )


def _face_str(self: cq.occ_impl.shapes.Face):
    return (
        "Face:{"
        + "Super:{"
        + _shape_str(self)
        + "}"
        + f", normal: {self.normalAt()}"
        + f", outter wire: {self.outerWire()}"
        + f", inner wires: {[repr(x) for x in self.innerWires()]}"
        + "}"
    )


def _comp_str(self: cq.occ_impl.shapes.Compound):
    return (
        "Compound:{"
        + "Super:{"
        + _shape_str(self)
        + "}"
        + f", children: {[repr(x) for x in self]}"
        + "}"
    )


def _edge_str(self: cq.occ_impl.shapes.Edge):
    return (
        "Edge:{"
        + "Super:{"
        + _shape_str(self)
        + "}"
        + f", forConstruction: {self.forConstruction}"
        + "}"
    )


def _wire_str(self: cq.occ_impl.shapes.Wire):
    return (
        "Wire: {"
        + "Super:{"
        + _shape_str(self)
        + "}"
        + f", start: {self.startPoint()}"
        + f", end: {self.endPoint()}"
        + "}"
    )


def _wp_str(self: cq.Workplane):
    objs = [str(x) for x in self.objects]
    return (
        "Workplane:[" + f"  parent: [{self.parent}]"
        if self.parent
        else "  no parent"
        + f", plane: {self.plane}"
        + f", objects: {objs}"
        + f", modelling context: {self.ctx}"
        + "]"
    )


cq.occ_impl.shapes.Shape.__repr__ = _shape_str
cq.occ_impl.shapes.Edge.__repr__ = _edge_str
cq.occ_impl.shapes.Compound.__repr__ = _comp_str
cq.occ_impl.shapes.Face.__repr__ = _face_str
cq.cq.CQContext.__repr__ = _ctx_str
cq.occ_impl.geom.BoundBox.__repr__ = _bb_str
cq.occ_impl.geom.Plane.__repr__ = _plane_str
cq.occ_impl.geom.Location.__repr__ = _loc_str
cq.Workplane.__repr__ = _wp_str
cq.occ_impl.shapes.Wire.__repr__ = _wire_str
