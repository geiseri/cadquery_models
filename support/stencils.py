from __future__ import annotations
from math import floor
import cadquery as cq
from typing import Tuple, Optional, Union

from .base_part import Base


class LCDStencil(Base):
    def __init__(self):
        super().__init__()
        self.radius = 0
        self.lcd = None

    def make(self):
        super().make()
        if self.radius == 0:
            self.lcd = (
                cq.Workplane("XY")
                .workplane()
                .box(self.size.x, self.size.y, self.size.z)
            )
        else:
            self.lcd = (
                cq.Workplane("XY")
                .workplane()
                .rounded_rect(
                    self.size.x,
                    self.size.y,
                    self.radius,
                )
                .workplane(offset=-(self.size.z / 4) * 3)
                .rect(
                    self.size.x - self.radius,
                    self.size.y - self.radius,
                )
                .workplane(offset=-(self.size.z / 4) * 1)
                .rect(self.size.x - self.radius, self.size.y - self.radius)
                .loft(True)
            )
        self.lcd = self.lcd.translate(-self.lcd.val().BoundingBox().center)

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = wp.union(self.lcd.val())
        return shape.translate(self.origin_offset)


class CylinderStencil(Base):
    def __init__(self):
        super().__init__()
        self.stencil = None

    def make(self):
        super().make()
        self.stencil = cq.Workplane("XY").cylinder(self.size.z, self.size.y / 2)

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = wp.union(self.stencil.val())
        return shape.translate(self.origin_offset)


class BoxStencil(Base):
    def __init__(self):
        super().__init__()
        self.stencil = None

    @classmethod
    def from_base(base: Base):
        res = BoxStencil()
        res.size = base.size

    def make(self):
        super().make()
        self.stencil = cq.Workplane("XY").box(self.size.x, self.size.y, self.size.z)

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = wp.union(self.stencil.val())
        return shape.translate(self.origin_offset)


class VentStencil(Base):
    def __init__(self):
        super().__init__()

        self.cols: int = 1
        self.rows: int = 1
        self.spacing: float = 1
        self.vent: cq.Workplane = None

    def make(self):
        super().make()
        self.vent = (
            cq.Workplane("XY")
            .workplane(offset=self.size.z)
            .placeSketch(self._slots())
            .extrude(-self.size.z, combine=True)
        )

        self.vent = self.vent.translate(-self.vent.val().BoundingBox().center)

    def cols_from_width(self, vent_width):
        self.cols = floor(self.size.x / (vent_width + self.spacing))

    def rows_from_height(self, vent_width):
        self.cols = floor(self.size.x / (vent_width + self.spacing))

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = wp.union(self.vent.val())
        # shape = wp.union(self.outline().val())
        return shape.translate(self.origin_offset)

    def _slots(self):
        vent_width = (self.size.x - ((self.cols - 1) * self.spacing)) / self.cols
        vent_height = (self.size.y - ((self.rows - 1) * self.spacing)) / self.rows

        if vent_width > vent_height:
            radius = min(1.3, vent_height / 3)
        else:
            radius = min(1.3, vent_width / 3)
        return (
            cq.Sketch()
            .rarray(
                self.spacing + vent_width,
                self.spacing + vent_height,
                self.cols,
                self.rows,
            )
            .rect(vent_width, vent_height, mode="a", tag="slot")
            .reset()
            .vertices(tag="slot")
            .fillet(radius)
        )
