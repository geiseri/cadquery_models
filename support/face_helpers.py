import cadquery as cq
from typing import Union, Literal, Callable, List, TypeVar, Tuple
import heatserts
from apply_to_each_face import (
    XAxisInPlane,
    WORLD_AXIS_PLANES_XY_ZX_YZ,
    WORLD_AXIS_PLANES_XY_YZ_ZX,
)


def _rounded_rect(
    self: cq.Workplane,
    xLen: float,
    yLen: float,
    fillet_radius: float,
    centered: Union[bool, Tuple[bool, bool]] = True,
    forConstruction: bool = False,
) -> cq.Workplane:
    if isinstance(centered, bool):
        centered = (centered, centered)

    offset = cq.Vector()
    if not centered[0]:
        offset += cq.Vector(xLen / 2, 0, 0)
    if not centered[1]:
        offset += cq.Vector(0, yLen / 2, 0)

    points = [
        cq.Vector(xLen / -2.0, yLen / -2.0, 0),
        cq.Vector(xLen / 2.0, yLen / -2.0, 0),
        cq.Vector(xLen / 2.0, yLen / 2.0, 0),
        cq.Vector(xLen / -2.0, yLen / 2.0, 0),
    ]

    points = [x + offset for x in points]

    # close the wire
    points.append(points[0])

    w = cq.Wire.makePolygon(points, forConstruction)
    w = w.fillet2D(fillet_radius, w.Vertices())
    return self.eachpoint(lambda loc: w.moved(loc), True)


cq.Workplane.rounded_rect = _rounded_rect


def _loft_round_hole(
    self: cq.Workplane, radius: float, padding: float, thickness: float
) -> cq.Workplane:
    r = (
        cq.Workplane("XY")
        .circle(radius + padding)
        .workplane(offset=thickness * 0.25)
        .circle(radius)
        .workplane(offset=thickness * 0.75)
        .circle(radius)
        .loft()
    ).val()

    def moved(loc: cq.Location):
        return r.moved(loc)

    return self.cutEach(moved, True, True)


cq.Workplane.loft_round_hole = _loft_round_hole


def _loft_rect_hole(
    self: cq.Workplane,
    x: float,
    y: float,
    padding: float,
    thickness: float,
    rounded: bool = False,
) -> cq.Workplane:
    if rounded:
        base = cq.Workplane("XY").rounded_rect(x + padding, y + padding, thickness / 8)
    else:
        base = cq.Workplane("XY").rect(x + padding, y + padding)

    r = (
        base.workplane(offset=thickness * 0.25)
        .rect(x + (padding / 2), y + (padding / 2))
        .workplane(offset=thickness * 0.25)
        .rect(x, y)
        .workplane(offset=thickness * 0.5)
        .rect(x, y)
        .loft()
    ).val()

    def moved(loc: cq.Location):
        return r.moved(loc)

    return self.cutEach(moved, True, True)


cq.Workplane.loft_rect_hole = _loft_rect_hole


def _cut_lcd_window(
    self: cq.Workplane,
    lcd_width: float,
    lcd_length: float,
    depth: float,
    radius: float = 0.01,
    x_offset: float = 0,
    y_offset: float = 0,
):
    def make_solid(wp, face):
        newCenter = face.Center() + self.plane.toWorldCoords((x_offset, y_offset))
        return (
            wp.add(face)
            .workplane(origin=newCenter)
            .rounded_rect(lcd_width + depth, lcd_length + depth, radius)
            .workplane(offset=-depth / 2)
            .rounded_rect(lcd_width, lcd_length, radius / 2)
            .workplane(offset=-depth / 2)
            .rect(lcd_width, lcd_length)
            .loft(combine="a")
        )

    return self.applyToEachFace(
        XAxisInPlane(WORLD_AXIS_PLANES_XY_YZ_ZX), make_solid, "cut"
    )


cq.Workplane.cut_lcd_window = _cut_lcd_window


def cut_lcd_from_face(
    face: cq.Face,
    lcd_width: float,
    lcd_length: float,
    depth: float,
    radius: float = 0.01,
    x_offset: float = 0,
    y_offset: float = 0,
):
    return (
        face.workplane()
        .rounded_rect(lcd_width + depth, lcd_length + depth, radius)
        .workplane(offset=-depth / 2)
        .rounded_rect(lcd_width, lcd_length, radius / 2)
        .workplane(offset=-depth / 2)
        .rect(lcd_width, lcd_length)
        .loft(combine="c")
    )


def _tag_faces(self: cq.Workplane, suffix) -> cq.Workplane:
    self.faces().tag(suffix)
    self.faces("|Y").tag("y_" + suffix)
    self.faces("|X").tag("x_" + suffix)
    self.faces("|Z").tag("z_" + suffix)
    self.faces(">Z").workplane().tag("z_max_" + suffix)
    self.faces("<Z").workplane().tag("z_min_" + suffix)
    self.faces(">Y").workplane().tag("y_max_" + suffix)
    self.faces("<Y").workplane().tag("y_min_" + suffix)
    self.faces(">X").workplane().tag("x_max_" + suffix)
    self.faces("<X").workplane().tag("x_min_" + suffix)
    return self


cq.Workplane.tag_faces = _tag_faces


def _cut_vents(
    self: cq.Workplane,
    vent_width: float,
    vent_length: float,
    vent_depth: float,
    cols: int,
    rows: int,
    spacing: float,
    x_offset: float = 0,
    y_offset: float = 0,
):
    def make_solid(wp: cq.Workplane, face: cq.Face):
        newCenter = self.plane.toLocalCoords(face.Center()) + cq.Vector(
            (x_offset, y_offset)
        )
        newCenter = self.plane.toWorldCoords(newCenter)
        s = (
            cq.Sketch()
            .rarray(spacing + vent_length, spacing + vent_width, cols, rows)
            .slot(vent_length - spacing, vent_width, mode="a")
            .reset()
        )

        return (
            wp.add(face)
            .workplane(origin=newCenter)
            .placeSketch(s)
            .extrude(-vent_depth, combine=True)
        )

    return self.applyToEachFace(
        XAxisInPlane(WORLD_AXIS_PLANES_XY_YZ_ZX), make_solid, "cut"
    )


cq.Workplane.cut_vents = _cut_vents
