from cadquery import (
    Solid,
    Location,
    Vector,
    Workplane,
    Plane,
    Compound,
    Selector,
    Wire,
    Shape,
    BoundBox,
)
import cadquery.occ_impl.shapes
from cadquery.cq import CQObject, VectorLike
from cadquery.selectors import DirectionMinMaxSelector
import cq_warehouse.extensions
from support import face_helpers
from typing import Union, Literal, Callable, List, TypeVar, Tuple
import support.face_helpers


class Case:
    def __init__(
        self,
        name: str,
        x: float,
        y: float,
        z: float,
        wall_thickness: float = 0,
        radius: float = 0,
    ):
        self.name = name
        self.x = x
        self.y = y
        self.z = z
        self.wall_thickness = wall_thickness
        self.radius = radius

        outter = Workplane("XY").box(self.x, self.y, self.z)
        if self.radius > 0:
            outter = outter.fillet(self.radius)
        inner = Workplane("XY").box(self.inner_x, self.inner_y, self.inner_z)
        if self.wall_thickness > 0:
            self.base_box = outter.cut(inner).val()

        self.side_lut = {
            "right": (self.right_box, "<<X", ">>X"),
            "left": (self.left_box, ">>X", "<<X"),
            "front": (self.front_box, ">>Y", "<<Y"),
            "back": (self.back_box, "<Y", ">Y"),
            "top": (self.top_box, "<<Z", ">>Z"),
            "bottom": (self.bottom_box, ">>Z", "<<Z"),
        }

    def __str__(self):
        return f"{self.name}, outside: {self.bounding_box}, inside: {self.inner_bounding_box}"

    @property
    def right(self) -> Workplane:
        return Workplane("YZ").workplane(
            offset=self.inner_x / 2, centerOption="CenterOfBoundBox"
        )

    @property
    def right_box(self) -> Workplane:
        return self.right.rect(self.y, self.z).extrude(self.wall_thickness)

    @property
    def right_side(self) -> Workplane:
        return self.right_box & self.base_box

    def resove_workplane(
        self, side: str, inner: bool, offset: float, keep_parent: bool
    ) -> Workplane:
        (base, face_inner, face_outter) = self.side_lut[side]
        if inner:
            face = face_inner
        else:
            face = face_outter
        base_workplane = base.faces(face).workplane(offset=offset, invert=False)
        if keep_parent:
            return base_workplane
        return Workplane(base_workplane.plane)

    def right_workplane(self, inner=False, offset=0, keep_parent=False) -> Workplane:
        return self.resove_workplane("right", inner, offset, keep_parent)

    @property
    def front(self) -> Workplane:
        return Workplane("XZ").workplane(
            offset=self.inner_y / 2, centerOption="CenterOfBoundBox"
        )

    @property
    def front_box(self) -> Workplane:
        return self.front.rect(self.x, self.z).extrude(self.wall_thickness)

    @property
    def front_side(self) -> Workplane:
        return self.front_box & self.base_box

    def front_workplane(self, inner=False, offset=0, keep_parent=False) -> Workplane:
        return self.resove_workplane("front", inner, offset, keep_parent)

    @property
    def top(self) -> Workplane:
        return Workplane("XY").workplane(
            offset=self.inner_z / 2, centerOption="CenterOfBoundBox"
        )

    @property
    def top_box(self) -> Workplane:
        return self.top.rect(self.x, self.y).extrude(self.wall_thickness)

    @property
    def top_side(self) -> Workplane:
        return self.top_box & self.base_box

    def top_workplane(self, inner=False, offset=0, keep_parent=False) -> Workplane:
        return self.resove_workplane("top", inner, offset, keep_parent)

    @property
    def left(self) -> Workplane:
        return Workplane("ZY").workplane(
            offset=self.inner_x / 2, centerOption="CenterOfBoundBox"
        )

    @property
    def left_box(self) -> Workplane:
        return self.left.rect(self.z, self.y).extrude(self.wall_thickness)

    @property
    def left_side(self) -> Workplane:
        return self.left_box & self.base_box

    def left_workplane(self, inner=False, offset=0, keep_parent=False) -> Workplane:
        return self.resove_workplane("left", inner, offset, keep_parent)

    @property
    def back(self) -> Workplane:
        return Workplane("ZX").workplane(
            offset=self.inner_y / 2, centerOption="CenterOfBoundBox"
        )

    @property
    def back_box(self) -> Workplane:
        return self.back.rect(self.z, self.x).extrude(self.wall_thickness)

    @property
    def back_side(self) -> Workplane:
        return self.back_box & self.base_box

    def back_workplane(self, inner=False, offset=0, keep_parent=False) -> Workplane:
        return self.resove_workplane("back", inner, offset, keep_parent)

    @property
    def bottom(self) -> Workplane:
        return Workplane("YX").workplane(
            offset=self.inner_z / 2, centerOption="CenterOfBoundBox"
        )

    @property
    def bottom_box(self) -> Workplane:
        return self.bottom.rect(self.y, self.x).extrude(self.wall_thickness)

    @property
    def bottom_side(self) -> Workplane:
        return self.bottom_box & self.base_box

    def bottom_workplane(self, inner=False, offset=0, keep_parent=False) -> Workplane:
        return self.resove_workplane("bottom", inner, offset, keep_parent)

    @property
    def inner_bounding_box(self) -> BoundBox:
        point0 = Vector(self.inner_x / 2, self.inner_y / 2, self.inner_z / 2)
        point1 = Vector(-self.inner_x / 2, -self.inner_y / 2, -self.inner_z / 2)
        return (point0, point1)

    @property
    def bounding_box(self) -> BoundBox:
        point0 = Vector(self.x / 2, self.y / 2, self.z / 2)
        point1 = Vector(-self.x / 2, -self.y / 2, -self.z / 2)
        return (point0, point1)

    @property
    def inner_x(self) -> float:
        return self.x - (self.wall_thickness * 2)

    @property
    def inner_y(self) -> float:
        return self.y - (self.wall_thickness * 2)

    @property
    def inner_z(self) -> float:
        return self.z - (self.wall_thickness * 2)

    def internal_intersect_die(self, source: Shape) -> CQObject:
        internal = Workplane("XY").box(self.inner_x, self.inner_y, self.inner_z)
        return Workplane("XY").add(source).intersect(internal).val()

    def internal_cut_die(self, source: Shape) -> CQObject:
        internal = Workplane("XY").box(self.inner_x, self.inner_y, self.inner_z)
        return Workplane("XY").add(source).cut(internal).val()

    def place(self, workplane: Workplane) -> Workplane:
        return workplane.add(self.base_box)


if __name__ == "__main__":
    from cq_server.ui import ui, show_object

    case = Case("test", 100, 100, 100, 3, 2)
    show_object(case)
