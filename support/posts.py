import cadquery as cq
from typing import T, Union, Literal
import heatserts
from apply_to_each_face import (
    XAxisInPlane,
    WORLD_AXIS_PLANES_XY_ZX_YZ,
    WORLD_AXIS_PLANES_XY_YZ_ZX,
)
from .base_part import Base

heatserts.heatsert_dims["M2"] = heatserts.dims(diam=3, depth=4, bolt_diam=2)


class Post:
    def __init__(self, width: float, height: float):
        self.width = width
        self.height = height

    def make_post(self, wp: cq.Workplane) -> cq.Workplane:
        return wp.box(self.width, self.width, self.height)

    def bounding_box(self):
        return cq.Vector(self.width, self.width, self.height)


class HeatsertPost(Post):
    def __init__(self, width: float, height: float, bolt_type):
        super().__init__(width, height)
        self.bolt_type = bolt_type
        self.clearance = height

    def bolt_diam(self) -> float:
        return heatserts.heatsert_dims[self.bolt_type].diam

    def make_post(self, wp: cq.Workplane) -> cq.Workplane:
        return (
            super()
            .make_post(wp)
            .faces(">Z")
            .heatsert(self.bolt_type, bolt_clear=self.clearance)
        )


class PinPost(Post):
    def __init__(
        self,
        width: float,
        height: float,
        pin_width: float,
        pin_height: float,
        type: Literal["male", "female", "m", "f"] = "male",
    ):
        if type not in {"male", "female", "m", "f"}:
            raise ValueError("Undefined type provided.")

        self.pin_width = pin_width
        self.pin_height = pin_height
        self.type = type
        super().__init__(width, height)

    def make_post(self, wp: cq.Workplane) -> cq.Workplane:
        return (
            wp.box(self.width, self.width, self.height - self.pin_height)
            .faces(">Z")
            .workplane()
            .circle(self.pin_width / 2)
            .extrude(self.pin_height)
        )

    def bounding_box(self):
        return cq.Vector(self.width, self.width, self.height + self.pin_height)


def _add_post(
    self: T,
    post: Post,
    combine: Union[bool, Literal["cut", "a", "s"]] = True,
    clean: bool = True,
) -> T:
    solid = post.make_post(cq.Workplane("XY")).val()

    def make_post(loc):
        return solid.located(loc)

    return self.eachpoint(make_post, True, combine, clean)


cq.Workplane.add_post = _add_post


def _post_plate(self: cq.Workplane, width, length, post: Post) -> cq.Workplane:
    solid = (
        cq.Workplane("XY")
        .rect(width, length, forConstruction=True)
        .vertices()
        .add_post(post)
        .val()
    )
    # print(solid)

    def apply_solid(loc):
        return solid.located(loc)

    return self.eachpoint(apply_solid, True)

    def surface_post(wp, face):
        return wp.rect(width, length, forConstruction=True).vertices().add_post(post)

    return self.applyToEachFace(XAxisInPlane(WORLD_AXIS_PLANES_XY_ZX_YZ), surface_post)


cq.Workplane.post_plate = _post_plate


class PostPart(Base):
    def __init__(self, post: Post):
        super().__init__()
        self.post = post
        self.size = post.bounding_box()

    def make(self) -> cq.Workplane:
        super().make()
        self.post_solid = self.post.make_post(cq.Workplane("XY"))
        self.post_solid = self.post_solid.translate(
            -self.post_solid.val().BoundingBox().center
        )

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = wp.add(self.post_solid)

        return shape.translate(self.origin_offset)


class PostSet(Base):
    def __init__(self):
        super().__init__()
        self.shape = None
        self.area_x = 0
        self.area_y = 0
        self.post = Post(5, 10)
        self.padding = 0

    def set_post_area(self, post: Post, xlen: float, ylen: float):
        self.post = post
        self.area_x = xlen
        self.area_y = ylen
        self.size.x = xlen + self.post.width
        self.size.y = ylen + self.post.width
        self.size.z = self.post.height

    def make(self):
        super().make()
        post = PostPart(self.post)
        post.size.z = post.size.z - self.padding
        post.origin_offset = cq.Vector(0, 0, self.size.z / 2)
        post.make()

        def mkpost(loc):
            return post.build().val().located(loc)

        pts = self._get_points()
        self.shape = pts.eachpoint(mkpost, combine=True)
        self.shape = self.shape.translate(-self.shape.val().BoundingBox().center)

    def _get_points(self) -> cq.Workplane:
        base = cq.Workplane("XY")
        if self.padding > 0:
            base = (
                base.rect(self.size.x, self.size.y)
                .extrude(self.padding)
                .faces("<Z")
                .workplane(invert=True)
            )

        if self.area_x == 0:
            pts = base.pushPoints([(0, -self.area_y / 2), (0, self.area_y / 2)])
        elif self.area_y == 0:
            pts = base.pushPoints([(-self.area_x / 2, 0), (self.area_x / 2, 0)])
        else:
            pts = base.rect(self.area_x, self.area_y, forConstruction=True).vertices()

        return pts

    @property
    def points(self):
        def convert_to(obj):
            if isinstance(obj, cq.Vector):
                return obj
            if isinstance(obj, cq.Vertex):
                return obj.Center()

        pts = self._get_points()
        return [convert_to(x) for x in pts.vals()]

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = wp.add(self.shape.val())
        return shape.translate(self.origin_offset)
