from __future__ import annotations
import cadquery as cq
from typing import Tuple, Optional, Union, Literal


class FaceRect:
    def __init__(
        self,
        face: Literal["left", "right", "top", "bottom", "front", "back"],
        bbox: cq.BoundBox,
    ):
        self.bbox = bbox
        self.face = face

    """
    Return the distance to make the current face's x side line up with the other face's x side
    """

    def align_x_offset(
        self, other: FaceRect, direction: Literal["left", "right"]
    ) -> float:
        (xlen, _) = self._2d()
        (other_xlen, _) = other._2d()
        # print(f"self {self}, other {other}, direction {direction}")
        if direction == "left":
            return (xlen / 2) - (other_xlen / 2)
        if direction == "right":
            return -((xlen / 2) - (other_xlen / 2))
        raise TypeError("bad direction")

    """
    Return the distance to make the current face's y side line up with the other face's y side
    """

    def __str__(self) -> str:
        return f"{self.face} ({self._2d()}), on {self.bbox}"

    def align_y_offset(
        self, other: FaceRect, direction: Literal["top", "bottom"]
    ) -> float:
        (_, ylen) = self._2d()
        (_, other_ylen) = other._2d()
        print(f"self {self}, other {other}, direction {direction}")

        if direction == "top":
            return -((ylen / 2) - (other_ylen / 2))
        if direction == "bottom":
            return (ylen / 2) - (other_ylen / 2)
        raise TypeError("bad direction")

    """
    def _2d(self) -> Tuple[float, float, float, float]:
        if self.face == "front":
            return (self.bbox.xlen, self.bbox.ylen)
        if self.face == "back":
            return (self.bbox.xlen, self.bbox.ylen)
        if self.face == "left":
            return (self.bbox.ylen, self.bbox.zlen)
        if self.face == "right":
            return (self.bbox.ylen, self.bbox.zlen)
        if self.face == "top":
            return (self.bbox.xlen, self.bbox.zlen)
        if self.face == "bottom":
            return (self.bbox.xlen, self.bbox.zlen)
    """

    def _2d(self) -> Tuple[float, float, float, float]:
        if self.face == "front":
            return (self.bbox.xlen, self.bbox.ylen)
        if self.face == "back":
            return (self.bbox.xlen, self.bbox.ylen)
        if self.face == "left":
            return (self.bbox.ylen, self.bbox.zlen)
        if self.face == "right":
            return (self.bbox.ylen, self.bbox.zlen)
        if self.face == "top":
            return (self.bbox.xlen, self.bbox.zlen)
        if self.face == "bottom":
            return (self.bbox.xlen, self.bbox.zlen)


def place_on_face(shape: cq.Solid, face: cq.Workplane) -> cq.Solid:
    if len(face.objects) > 0:
        face = face.workplane()
    return shape.located(face.plane.location)


def align_inside(
    first: Union[Base, cq.Vector, cq.BoundBox],
    other: Union[Base, cq.Vector, cq.BoundBox],
    direction: Tuple[int, int, int],
    adjust: cq.Vector = cq.Vector(0, 0, 0),
) -> cq.Vector:
    """
    start at center
    calculate translation * dir to get offset
    """

    if isinstance(first, cq.Vector):
        first_size = first
    elif isinstance(first, cq.BoundBox):
        first_size = cq.Vector(first.xlen, first.ylen, first.zlen)
    else:
        first_size = first.bounding_box()

    if isinstance(other, cq.Vector):
        other_size = other
    elif isinstance(other, cq.BoundBox):
        other_size = cq.Vector(other.xlen, other.ylen, other.zlen)
    else:
        other_size = other.bounding_box()

    print(f"size 1: {first_size} size 2: {other_size}")

    x_dir, y_dir, z_dir = direction

    print(f"first before: {first_size}, after {first_size/2}")
    print(f"other before: {other_size}, after {other_size/2}")
    offset = (first_size / 2) - (other_size / 2)
    offset.x = (offset.x * x_dir) + (adjust.x * x_dir)
    offset.y = (offset.y * y_dir) + (adjust.y * y_dir)
    offset.z = (offset.z * z_dir) + (adjust.z * z_dir)
    print(f"offset: {offset}, orig self: {first_size}, other self: {other_size}")

    return offset


class Base:
    def __init__(self):
        self.make_called = False
        self.size = cq.Vector(0, 0, 0)
        self.origin_offset = cq.Vector(0, 0, 0)

    def make(self):
        self.make_called = True

    def build(self) -> cq.Workplane:
        if self.make_called == False:
            raise Exception("Make has not been called")
        return cq.Workplane("XY")

    def bounding_box(self):
        box = self.outline().val().BoundingBox()
        return cq.Vector(box.xlen, box.ylen, box.zlen)

    def outline(self) -> cq.Workplane:
        return cq.Workplane("XY").box(self.size.x, self.size.y, self.size.z)

    def outside_workplane(
        self,
        side: Literal["left", "right", "top", "bottom", "front", "back"],
        offset: float = 0.0,
        invert: bool = False,
        centerOption: Literal[
            "CenterOfMass", "ProjectedOrigin", "CenterOfBoundBox"
        ] = "ProjectedOrigin",
        origin: Optional[cq.Vector] = None,
    ) -> cq.Workplane:
        def workplane_for(face: cq.Workplane) -> cq.Workplane:
            return cq.Workplane(face.workplane().plane)

        return workplane_for(self.outline().faces(side)).workplane(
            offset, invert, centerOption, origin
        )

    def face_rect(
        self, side: Literal["left", "right", "top", "bottom", "front", "back"]
    ) -> FaceRect:
        return FaceRect(side, self.outline().val().BoundingBox())

    def place(self, wp: cq.Workplane) -> cq.Shape:
        built = self.build()
        if len(wp.objects) > 0:
            wp = wp.workplane()
        print(f"transform: {wp.plane.location.toTuple()}\n, old: {built.val()}")
        built: cq.Workplane = built.translate(-built.val().Center())
        print(f"new bb: {built.val()}")

        return built.val().located(wp.plane.location).translate(self.origin_offset)


class BaseShell(Base):
    def __init__(self):
        super().__init__()
        self.scene = None
        self.radius = 0.0
        self.smooth = False
        self.wall_thickness = 1.0

    def from_internal_size(self, size: cq.Vector, wall_thickness: float):
        self.wall_thickness = wall_thickness
        self.size.x = size.x + (wall_thickness * 2)
        self.size.y = size.y + (wall_thickness * 2)
        self.size.z = size.z + (wall_thickness * 2)

    def inside_workplane(
        self,
        side: Literal["left", "right", "top", "bottom", "front", "back"],
        offset: float = 0.0,
        invert: bool = False,
        centerOption: Literal[
            "CenterOfMass", "ProjectedOrigin", "CenterOfBoundBox"
        ] = "ProjectedOrigin",
        origin: Optional[cq.Vector] = None,
    ):
        box = cq.Workplane("XY").box(
            self.size.x - (self.wall_thickness * 2),
            self.size.y - (self.wall_thickness * 2),
            self.size.z - (self.wall_thickness * 2),
        )

        def workplane_for(face: cq.Workplane) -> cq.Workplane:
            print("size: {}".format(face.size()))
            return cq.Workplane(face.workplane().plane)

        return workplane_for(box.faces(side)).workplane(
            offset, invert, centerOption, origin
        )

    def make(self):
        super().make()
        outter = cq.Workplane("XY").box(self.size.x, self.size.y, self.size.z)
        if self.radius > 0:
            if self.smooth:
                outter = outter.fillet(self.radius)
            else:
                outter = outter.chamfer(self.radius)
        inner_size = self.size - cq.Vector(
            self.wall_thickness * 2, self.wall_thickness * 2, self.wall_thickness * 2
        )
        inner = cq.Workplane("XY").box(inner_size.x, inner_size.y, inner_size.z)
        if self.wall_thickness > 0:
            self.scene = outter.cut(inner)
        else:
            self.scene = outter

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = wp.union(self.scene)
        return shape.translate(self.origin_offset)
