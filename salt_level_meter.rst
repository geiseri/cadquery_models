=============================
Watersoftener Monitor Project
=============================

`Source <https://gitlab.com/geiseri/cadquery_models/-/blob/main/salt_level_meter.py>`_

Top Part
--------

:download:`3D print <_static/salt_level_meter_top.3mf>`

.. cadquery-vtk::

    from salt_level_meter import top_part_final
    show_object(top_part_final)



Bottom Part
-----------

:download:`3D print <_static/salt_level_meter_bottom.3mf>`

.. cadquery-vtk::

    from salt_level_meter import bottom_part_final
    show_object(bottom_part_final)

