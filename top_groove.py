# from cq_server.ui import ui, show_object
if __name__ == "__main__":
    from ocp_vscode import (
        show,
        show_object,
        reset_show,
        set_port,
        set_defaults,
        get_defaults,
    )

    set_port(3939)


import cadquery as cq
import cq_warehouse.extensions
from cq_warehouse.drafting import Draft
import heatserts as heatserts
from cadquery import exporters
import support.debugging


sk: cq.Sketch = (
    cq.Sketch()
    .arc((0.0, 0.0), 0.5, 0.0, 360.0)
    .arc((0.5, 1.0), 0.5, 0.0, 360.0)
    .arc((0.0, 1.5), 0.5, 0.0, 360.0)
    .segment((0.0, 0.750), (0, 1))
    .hull()
)


def dump_verts(prefix, obj):
    verts = obj.Vertices()
    inc = 1 / len(verts)
    col = 128
    show_object(
        [cq.Workplane("XY", origin=(x.X, x.Y, x.Z)).box(0.5, 0.5, 0.5) for x in verts],
        name=f"{prefix}",
        default_color=(col, 0, col, 1),
        default_edgecolor=(col, 0, col, 1),
    )
    # for idx, x in enumerate(
    #     [cq.Workplane("XY", origin=(x.X, x.Y, x.Z)).box(0.5, 0.5, 0.5) for x in verts]
    # ):
    #     # print(f"{prefix}-{idx} {x.val().Center()}")
    #     show_object(
    #         x,
    #         name=f"{prefix}-{idx}",
    #         default_color=(1 - col, 0, col, 1),
    #         default_edgecolor=(1 - col, 0, col, 1),
    #     )
    #     col = col + inc


box: cq.Workplane = cq.Workplane("XY").box(20, 30, 10).shell(2).split(keepBottom=True)
wire = box.faces(">Z").wires(cq.selectors.LengthNthSelector(-1))
wire2 = wire.toPending().offset2D(-0.25)
# dump_verts("faces", box.val())
# print(f"wire2 {wire2}")

face1: cq.Face = cq.Face.makeFromWires(wire.val(), [wire2.val()])
sld = face1.thicken(0.1)

# for idx,edge in enumerate(wire.val().Edges()):
#    print(f"edge1 {idx} is {edge.Vertices()}")

# for idx,edge in enumerate(wire2.val().Edges()):
#        print(f"edge2 {idx} is {edge.Vertices()}")

verts = wire.val().Vertices()

# inc =  1/len(verts)
# col = 0
# for idx,x in enumerate([cq.Workplane("XY").moveTo(x.X, x.Y).box(0.5,0.5,0.5) for x in verts]):
#    show_object(x, name=f"point-{idx}", options={"color": cq.Color(1-col, 0, col, 1)})
#    col = col + inc


vert = wire2.vertices("<X").vertices("<Y")
plineSweep: cq.Workplane = cq.Workplane("XZ", origin=vert.val().toTuple()).placeSketch(
    sk
)
plineSweep = plineSweep.sweep(wire2)
# show_object(vert_objs, name="verts")
# show_object(plineSweep, name="pipe", options={"color": cq.Color(0, 1, 1, 0.5)})
# show_object(box, name="box", options={"color": cq.Color(0, 0, 1, 0.1)})
# show_object(sld, name="face", options={"color": cq.Color(0,1, 0)})

# show_object(dump_verts("wire2", wire2.val()), name="wire2")


if __name__ == "__main__":
    dump_verts("wire", wire.val())
    show_object(plineSweep, name="sweep")

if __name__ == "__cqgi__":
    show_object(plineSweep)
