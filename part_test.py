import importlib
from ocp_vscode import (
    show,
    show_object,
    reset_show,
    set_port,
    set_defaults,
    get_defaults,
)

set_port(3939)

# from cq_server.ui import ui, show_object
import cadquery as cq
import support.base_part
from support.base_part import Base, BaseShell, align_inside
from support.stencils import LCDStencil, Rectangle

import support.face_helpers
import support.debugging
import heatserts
from support.posts import PostPart, HeatsertPost
from typing import Tuple, Optional, Union

importlib.reload(support.base_part)
importlib.reload(support.face_helpers)


def workplane_for(face: cq.Workplane) -> cq.Workplane:
    print("size: {}".format(face.size()))
    return cq.Workplane(face.workplane().plane)


def place_on_face(shape: cq.Solid, face: cq.Workplane):
    if len(face.objects) > 0:
        face = face.workplane()
    return shape.located(face.plane.location)


class SimpleCase(Base):
    def __init__(self):
        super().__init__()

        self.hole_diameter = 0
        self.scene = None
        self.cut_hole = None
        self.side = None

    def make(self):
        super().make()
        self.scene = cq.Workplane("XY").box(self.size.x, self.size.y, self.size.z)
        self.scene = self.scene.shell(-2, "arc")
        self.scene = self.scene.fillet(5)

        self.cut_hole = Rectangle("right")
        self.cut_hole.size = cq.Vector(self.hole_diameter, self.hole_diameter / 2, 2)
        self.cut_hole.origin_offset = align_inside(
            self, self.cut_hole, (0, 0, -1), cq.Vector(0, 0, 0)
        )
        self.cut_hole.make()

        self.cut_hole_left = Rectangle("left")
        self.cut_hole_left.size = cq.Vector(
            self.hole_diameter, self.hole_diameter / 2, 2
        )
        self.cut_hole_left.origin_offset = align_inside(
            self, self.cut_hole_left, (1, 0, 0), cq.Vector(0, 0, 0)
        )
        self.cut_hole_left.make()

        self.side = place_on_face(
            cq.Workplane("XY").text("bottom", 12, 5).val(),
            self.outside_workplane("bottom"),
        )
        self.side1 = place_on_face(
            cq.Workplane("XY").text("right", 12, 5).val(),
            self.outside_workplane("right"),
        )
        self.side2 = place_on_face(
            cq.Workplane("XY").text("top", 12, 5).val(), self.outside_workplane("top")
        )
        self.side3 = place_on_face(
            cq.Workplane("XY").text("left", 12, 5).val(),
            self.outside_workplane(
                "left",
            ),
        )
        self.side4 = place_on_face(
            cq.Workplane("XY").text("front", 12, 5).val(),
            self.outside_workplane(
                "front",
            ),
        )
        self.side5 = place_on_face(
            cq.Workplane("XY").text("back", 12, 5).val(),
            self.outside_workplane(
                "back",
            ),
        )

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = (
            wp.union(self.scene)
            .union(self.cut_hole.build())
            .cut(self.cut_hole_left.build())
            .union(self.side)
            .union(self.side1)
            .union(self.side2)
            .union(self.side3)
            .union(self.side4)
            .union(self.side5)
        )
        return shape.translate(self.origin_offset)


class ComplexCase(BaseShell):
    def __init__(self):
        super().__init__()
        self.a = None
        self.b = None
        self.post = None
        self.lcd = None
        self.lcd_size = cq.Vector(1, 1, 1)

    def make(self):
        super().make()

        self.a = PostPart(HeatsertPost(10, 10, "M2"))
        print(f"size: {self.a.size}, wall: {self.wall_thickness}")
        self.a.origin_offset = cq.Vector(0, 0, +(self.a.size.z / 2))
        self.a.make()

        self.b = PostPart(HeatsertPost(5, self.size.z / 2, "M2"))
        self.b.origin_offset = cq.Vector(0, +(self.b.size.z / 2), 0)
        self.b.make()

        self.lcd = LCDStencil()
        self.lcd.radius = self.wall_thickness * 2
        self.lcd.size = cq.Vector(
            self.lcd_size.x + self.lcd.radius,
            self.lcd_size.y + self.lcd.radius,
            self.wall_thickness,
        )
        self.lcd.origin_offset = cq.Vector(0, -(self.lcd.size.z / 2), 0)
        self.lcd.make()

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = (
            wp.union(self.a.place(self.inside_workplane("back", invert=True)))
            .union(self.b.place(self.inside_workplane("bottom", invert=True)))
            .union(self.lcd.place(self.inside_workplane("top", invert=False)))
        )
        return shape.translate(self.origin_offset)


blah = 1

if blah == 1:
    cc = SimpleCase()
    cc.size.y = 100
    cc.size.z = 50
    cc.size.x = 55
    cc.hole_diameter = 15
    cc.origin_offset = (-10, 0, 0)
elif blah == 2:
    cc = ComplexCase()
    cc.size.y = 100
    cc.size.z = 50
    cc.size.x = 55
    cc.origin_offset = (0, 0, 0)
    cc.wall_thickness = 3
    cc.hole_diameter = 15
    cc.lcd_size.x = 25
    cc.lcd_size.y = 30
    cc.lcd_size.z = 3
    cc.radius = 1.5
    cc.smooth = True
elif blah == 4:
    cc = BaseShell()
    cc.size.y = 100
    cc.size.z = 50
    cc.size.x = 55
    cc.origin_offset = (-5, 0, 0)
    cc.wall_thickness = 2
    cc.radius = 3
elif blah == 5:
    cc = BaseShell()
    cc.from_internal_size(cq.Vector(60, 110, 60), 3)
    cc.radius = 1.5

cc.make()
case = cc.build()


show(case, names=["Case"], colors=[(255, 1, 1)])
