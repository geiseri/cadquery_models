=======================
AQI Sensor Project Case
=======================

`Source <https://gitlab.com/geiseri/cadquery_models/-/blob/main/aqi_case.py>`_

Lid
---

:download:`3D print <_static/aqi_case_lid.3mf>`

.. cadquery-vtk::

    from aqi_case import lid
    show_object(lid)




Bottom
------

:download:`3D print <_static/aqi_case_bottom.3mf>`

.. cadquery-vtk::

    from aqi_case import bottom
    show_object(bottom)


Model
-----
