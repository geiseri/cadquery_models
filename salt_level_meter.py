if __name__ == "__main__":
    from ocp_vscode import (
        show,
        show_object,
        reset_show,
        set_port,
        set_defaults,
        get_defaults,
    )

    set_port(3939)

print(f"name is: `{__name__}`")

import cadquery as cq
import cq_warehouse.extensions
from cq_warehouse.drafting import Draft
import heatserts as heatserts
from cadquery import exporters
import support.case
import support.face_helpers
import support.posts
from support.case import Case
from support.face_helpers import cut_lcd_from_face
from support.posts import HeatsertPost, PinPost
import support.debugging


def align_edge(outlen: float, inlen: float, direction: str, offset: float = 0):
    diff = (outlen / 2) - (inlen / 2)
    if direction == "left":
        return -diff + offset
    return diff + offset


def create_mcu_posts(
    box: Case, geometry: cq.Vector, bottom_z: float, hole_pos, standoff: PinPost
):
    dummy = (
        box.front_workplane(inner=True, offset=bottom_z)
        .center(align_edge(box.inner_x, geometry.x, "left", 9), 0)
        .rect(geometry.x, geometry.y)
        .extrude(geometry.z)
    )

    center = dummy.val().BoundingBox().center
    print("point at", center)
    posts = (
        box.front_workplane(inner=True)
        .center(-center.x, -center.y)
        .pushPoints(hole_pos)
        .add_post(standoff)
    )

    return (posts, dummy)


def create_pcb_posts(
    box: Case,
    geometry: cq.Vector,
    pcb_bottom: float,
    offset: float,
    hole_pos,
    standoff: PinPost,
    slop,
):
    dummy = (
        box.bottom_workplane(inner=True, offset=pcb_bottom)
        .center(
            align_edge(box.inner_x, geometry.x, "left", -slop),
            align_edge(box.inner_y, geometry.y, "left", offset),
        )
        .rect(geometry.x, geometry.y)
        .extrude(geometry.z)
    )

    center = dummy.val().BoundingBox().center

    posts = (
        box.bottom_workplane(inner=True)
        .center(center.x, center.y)
        .pushPoints(hole_pos)
        .add_post(standoff)
    )
    return (posts, dummy)


def derive_workplane(current: cq.Workplane, next: cq.Workplane) -> cq.Workplane:
    orig = cq.Plane(next.plane.origin, current.plane.xDir, current.plane.zDir)
    return cq.Workplane(orig)


cq.Workplane.derive_workplane = derive_workplane


width = 80.0
thickness = 10.0
diameter = 22.0
padding = 12.0

slop = 0.1
wall_thickness = 2.5
back_height = 40.0

power_jack = cq.Vector(14.2, 50, 12)
power_jack_hole = 10
pcb_geometry = cq.Vector(50, 43, 1)
pcb_hole = 3.5
# 23.6 + 3.5 + 23.6
pcb_x_cross = [23.6, 3.5, 23.6]
# 2.15 + 3.5 + 32 + 3.5 + 2.15
pcb_y_cross = [2.15, 3.5, 32, 3.5, 2.15]
pcb_hole_pos = [
    (
        (pcb_x_cross[0] + (pcb_x_cross[1] / 2)) - (pcb_geometry.x / 2),
        (pcb_y_cross[0] + (pcb_y_cross[1] / 2)) - (pcb_geometry.y / 2),
    ),
    (
        (pcb_x_cross[0] + (pcb_x_cross[1] / 2)) - (pcb_geometry.x / 2),
        (pcb_y_cross[0] + pcb_y_cross[1] + pcb_y_cross[2] + (pcb_y_cross[3] / 2))
        - (pcb_geometry.y / 2),
    ),
]
pcb_top_z = 14.1
pcb_bot_z = 6.9
mcu_geometry = cq.Vector(31.17, 40, 1)
mcu_bottom_z = 5
mcu_offset = 5
mcu_hole = 2

# edge_l -> hole_l -> hole_r -> edge_r
mcu_cross = [1.5, mcu_hole, 24, mcu_hole, 1.5]
mcu_hole_pos = [
    (
        (mcu_cross[0] + (mcu_cross[1] / 2)) - (mcu_geometry.x / 2),
        (1.5 - (mcu_geometry.y / 2)),
    ),
    (
        (mcu_cross[0] + mcu_cross[1] + mcu_cross[2] + (mcu_cross[3] / 2))
        - (mcu_geometry.x / 2),
        (1.5 - (mcu_geometry.y / 2)),
    ),
]

lcd_geometry = cq.Vector(29.1, 13.75, 2)
lcd_pcb_geometry = cq.Vector(29.1, 25.9, 2)
lcd_pcb_hole = 2.5
lcd_pcb_y_cross = [7.2, lcd_pcb_hole, 2, 14]  # lcd start  # lcd end
lcd_pcb_x_cross = [1.4, lcd_pcb_hole, 21.3, lcd_pcb_hole, 1.4]
lcd_pcb_hole_pos = [
    (
        lcd_pcb_x_cross[0] + (lcd_pcb_x_cross[1] / 2),
        lcd_pcb_y_cross[0] + (lcd_pcb_y_cross[1] / 2),
    ),
    (
        lcd_pcb_x_cross[0]
        + lcd_pcb_x_cross[1]
        + lcd_pcb_x_cross[2]
        + (lcd_pcb_x_cross[3] / 2),
        lcd_pcb_y_cross[0] + (lcd_pcb_y_cross[1] / 2),
    ),
]
lcd_offset = lcd_pcb_y_cross[0] + lcd_pcb_y_cross[1] + lcd_pcb_y_cross[2]
lcd_post_offset = -(lcd_pcb_geometry.x / 2)
lcd_pcb_z = 4.25

print(
    "lcd_pcb_y_cross: {} lcd_pcb_x_cross: {} lcd_offset {}, lcd_post_offset: {}".format(
        sum(lcd_pcb_y_cross), sum(lcd_pcb_x_cross), lcd_offset, lcd_post_offset
    )
)

pcb_back_offset = mcu_bottom_z + mcu_geometry.z + 5

main_box = Case(
    "main_box",
    pcb_geometry.x + (wall_thickness * 2) + 10,
    pcb_geometry.y + (wall_thickness * 2) + power_jack.x + pcb_back_offset * 2,
    pcb_top_z + back_height + (wall_thickness * 2),
    wall_thickness,
    1.5,
)

scene = main_box.place(cq.Workplane("XY"))

post_screw = "M3"

post = HeatsertPost(
    heatserts.heatsert_dims[post_screw].diam + (wall_thickness),
    heatserts.heatsert_dims[post_screw].depth + 3,
    post_screw,
)

top_post_pos = (
    main_box.front.workplane(offset=-post.height)
    .center(
        align_edge(main_box.inner_x, post.width, "left"),
        align_edge(main_box.inner_z, post.width, "right"),
    )
    .hLine(main_box.inner_x - post.width, forConstruction=True)
    .vertices()
)

posts = top_post_pos.add_post(post)

post_holes = main_box.front_box - main_box.front_workplane(
    True, keep_parent=True
).pushPoints([(x.X, x.Z, 0) for x in top_post_pos.vals()]).cboreHole(
    heatserts.heatsert_dims[post_screw].bolt_diam,
    5,
    (wall_thickness / 2),
    (wall_thickness),
)

print("top_post: {}".format([(-x.X, x.Y, 0) for x in top_post_pos.vals()]))

powerjack_dummy = (
    main_box.right.workplane()
    .center(
        align_edge(main_box.inner_y, power_jack.x, "right", -2),
        align_edge(main_box.inner_z, power_jack.z, "left"),
    )
    .circle(power_jack.z / 2)
    .extrude(-power_jack.y)
)


pcb_posts, pcb_dummy = create_pcb_posts(
    main_box,
    pcb_geometry,
    pcb_bot_z,
    pcb_back_offset,
    pcb_hole_pos,
    PinPost(5, pcb_bot_z, pcb_hole, 3, "male"),
    slop,
)
mcu_posts, mcu_dummy = create_mcu_posts(
    main_box,
    mcu_geometry,
    mcu_bottom_z,
    mcu_hole_pos,
    PinPost(5, mcu_bottom_z, mcu_hole, 3, "male"),
)

lcd_pcb = (
    main_box.top_workplane(offset=lcd_pcb_z, inner=True)
    .center(
        0,
        align_edge(main_box.inner_y, lcd_pcb_geometry.y, "left", pcb_back_offset + 25),
    )
    .rect(lcd_pcb_geometry.x, lcd_pcb_geometry.y)
    .extrude(lcd_pcb_geometry.z)
)

lcd_pcb_center = lcd_pcb.val().BoundingBox().center

lcd_pcb_posts = (
    derive_workplane(lcd_pcb, main_box.top)
    .center(lcd_post_offset, 3 / 2)
    .pushPoints(lcd_pcb_hole_pos)
    .add_post(PinPost(lcd_pcb_hole + 1, lcd_pcb_z + slop, lcd_pcb_hole, 3, "male"))
)
print("lcd posts", lcd_pcb_posts.val().BoundingBox())

lcd_window_start = derive_workplane(lcd_pcb, main_box.top).workplane(
    offset=-wall_thickness
)
lcd_window_radius = 0.3
lcd_window_width = wall_thickness + 1
lcd = (
    lcd_window_start.workplane()
    .rounded_rect(
        lcd_geometry.x + lcd_window_width,
        lcd_geometry.y + lcd_window_width,
        lcd_window_radius,
    )
    .workplane(offset=wall_thickness / 2)
    .rounded_rect(lcd_geometry.x, lcd_geometry.y, lcd_window_radius / 2)
    .workplane(offset=wall_thickness / 2)
    .rect(lcd_geometry.x, lcd_geometry.y)
    .loft()
)


top_cut = (
    main_box.top.workplane(offset=main_box.wall_thickness)
    .center(0, main_box.wall_thickness)
    .rect(main_box.x, main_box.y)
    .extrude(-main_box.z / 2, combine=False)
)
bottom_part = scene.cut(top_cut)

point0, point1 = main_box.inner_bounding_box
point1.z = 0
point1.y = 0

faces = bottom_part.faces(cq.selectors.BoxSelector([point0], [point1]))
lip = faces.thicken(3).intersect(
    cq.Workplane("XY")
    .rect(main_box.inner_x + wall_thickness, main_box.inner_y + wall_thickness)
    .extrude(5, combine=False)
)
lip_slop = faces.thicken(3).intersect(
    cq.Workplane("XY")
    .rect(
        main_box.inner_x + wall_thickness + (slop * 2),
        main_box.inner_y + wall_thickness + (slop * 2),
    )
    .extrude(5, combine=False)
)

bottom_part = bottom_part + lip
top_part = scene.cut(bottom_part)
top_part = top_part - lip_slop


bottom_part_final = bottom_part + pcb_posts + mcu_posts - post_holes
top_part_final = top_part + posts + lcd_pcb_posts - lcd

views = [
    (main_box.right_side, "box right", {"color": (0, 0, 1, 0.25)}),
    (
        main_box.right_workplane(inner=True, offset=5).rect(5, 15).extrude(5),
        "right inner",
        {"color": (0, 0, 1, 0.8)},
    ),
    (
        main_box.right_workplane(inner=False, offset=5).rect(5, 15).extrude(5),
        "right outter",
        {"color": (0, 0, 1, 0.8)},
    ),
    (main_box.left_side, "box left", {"color": (0, 0.5, 1, 0.25)}),
    (
        main_box.left_workplane(inner=True, offset=5).rect(5, 15).extrude(5),
        "left inner",
        {"color": (0, 0.5, 1, 0.8)},
    ),
    (
        main_box.left_workplane(inner=False, offset=5).rect(5, 15).extrude(5),
        "left outter",
        {"color": (0, 0.5, 1, 0.8)},
    ),
    (main_box.top_side, "box top", {"color": (0, 1, 0.5, 0.25)}),
    (
        main_box.top_workplane(inner=True, offset=5).rect(5, 15).extrude(5),
        "top inner",
        {"color": (0, 1, 0.5, 0.8)},
    ),
    (
        main_box.top_workplane(inner=False, offset=5).rect(5, 15).extrude(5),
        "top outter",
        {"color": (0, 1, 0.5, 0.8)},
    ),
    (main_box.bottom_side, "box bottom", {"color": (0.5, 1, 0, 0.25)}),
    (
        main_box.bottom_workplane(inner=True, offset=5).rect(5, 15).extrude(5),
        "bottom inner",
        {"color": (0.5, 1, 0, 0.8)},
    ),
    (
        main_box.bottom_workplane(inner=False, offset=5).rect(5, 15).extrude(5),
        "bottom outter",
        {"color": (0.5, 1, 0, 0.8)},
    ),
    (main_box.front_side, "box front", {"color": (1, 0.5, 0, 0.25)}),
    (
        main_box.front_workplane(inner=True, offset=5).rect(5, 15).extrude(5),
        "front inner",
        {"color": (1, 0.5, 0, 0.8)},
    ),
    (
        main_box.front_workplane(inner=False, offset=5).rect(5, 15).extrude(5),
        "front outter",
        {"color": (1, 0.5, 0, 0.8)},
    ),
    (main_box.back_side, "box back", {"color": (1, 0, 0, 0.25)}),
    (
        main_box.back_workplane(inner=True, offset=5).rect(5, 15).extrude(5),
        "back inner",
        {"color": (1, 0, 0, 0.8)},
    ),
    (
        main_box.back_workplane(inner=False, offset=5).rect(5, 15).extrude(5),
        "back outter",
        {"color": (1, 0, 0, 0.8)},
    ),
]


full_model = bottom_part_final + top_part_final

if __name__ == "__main__":
    # for x in views:
    #   show_object(x[0],name=x[1],options=x[2])
    show_object(bottom_part_final, name="Bottom part")
    show_object(top_part_final, name="Top part")
    show_object(full_model, name="Full Model")

if __name__ == "__cqgi__":
    show_object(bottom_part_final)
    show_object(top_part_final)
    show_object(full_model)
