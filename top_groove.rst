==============
Demo of sweeps
==============

`Source <https://gitlab.com/geiseri/cadquery_models/-/blob/main/top_groove.py>`_


Sweep Shape
-----------

:download:`3D print <_static/top_groove.3mf>`

.. cadquery-vtk:: 
    
    from top_groove import plineSweep
    show_object(plineSweep)
