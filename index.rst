===============
Cadquery Models
===============

.. toctree::
   :maxdepth: 2
   :caption: Contents:


AQI Sensor Project Case
-----------------------

:doc:`aqi_case`

.. cadquery-svg:: 

    from aqi_case import model
    show_object(model)

Sweep Test
----------

:doc:`top_groove`

.. cadquery-svg:: 

    from top_groove import plineSweep
    show_object(plineSweep)

Watersoftener Monitor Project (in progress)
-------------------------------------------

:doc:`salt_level_meter`

.. cadquery-svg::

    from salt_level_meter import full_model
    show_object(full_model)


