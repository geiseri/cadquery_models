if __name__ == "__main__":
    from ocp_vscode import (
        show,
        show_object,
        reset_show,
        set_port,
        set_defaults,
        get_defaults,
    )

    set_port(3939)

from math import ceil, floor

import cadquery as cq
from cadquery import exporters
from cadquery import selectors as selectors

from support.base_part import Base, BaseShell, align_inside, place_on_face
from support.stencils import LCDStencil, VentStencil, CylinderStencil, BoxStencil
from support.posts import HeatsertPost, PinPost, PostPart

import support.face_helpers
import support.debugging
import heatserts
from support.posts import PostPart, HeatsertPost, Post, PostSet
from typing import Tuple, Optional, Union, T

import cq_warehouse.extensions
from cq_warehouse.drafting import Draft


def optimize_print(line, dim):
    res = 0
    if isinstance(dim, cq.Vector):
        res = cq.Vector(ceil(dim.x / line) * line, ceil(dim.y / line) * line, dim.z)
    else:
        res = ceil(dim / line) * line
    print(f"Input was {dim}, output is {res} with linewidth {line}")
    return res


metric_drawing = Draft(decimal_precision=1)

print_line_width = 0.52


lid_post_width = optimize_print(print_line_width, 6)
wall_thickness = optimize_print(print_line_width, 1) * 2
lid_depth = wall_thickness

main_pcb = cq.Vector(44.48, 52.06, 1.56)
main_pcb_right_offset = 15
# main_pcb_top_offset = 1.5  # todo: make dynamic
main_pcb_top_offset = 0  # todo: make dynamic
main_pcb_bottom_padding = 0
main_pcb_post_width = optimize_print(print_line_width, 7)
main_pcb_posts_padding = 10

esp32_pcb = cq.Vector(30, 40, 2)
esp32_pcb_top_offset = 1.75  # todo: make dynamic
esp32_pcb_gap = 20


pms5003 = cq.Vector(40, 50, 22)
esp32_post = HeatsertPost(6, pms5003.z + 10, "M2")

esp32_pms5003_y_gap = 5
pms5003_support_width = optimize_print(print_line_width, 2)


if pms5003.y + esp32_pms5003_y_gap + lid_post_width < (
    main_pcb.y + main_pcb_bottom_padding
):
    esp32_pms5003_y_gap = (main_pcb.y + main_pcb_bottom_padding) - pms5003.y
    print("use main pcb")
## FIXME: make sure we have enough room
# if esp32_pms5003_y_gap < esp32_post.width + pms5003_support_width:
#    esp32_pms5003_y_gap = esp32_post.width
#    print("use esp post width")
# if esp32_pms5003_y_gap < lid_post_width + pms5003_support_width:
#    esp32_pms5003_y_gap = lid_post_width
#    print("use case post width")

if pms5003.y + esp32_pms5003_y_gap + lid_post_width < (
    main_pcb.y + main_pcb_bottom_padding
):
    ylen = main_pcb.y + main_pcb_bottom_padding
else:
    ylen = pms5003.y + esp32_pms5003_y_gap + lid_post_width + (pms5003_support_width)
internal_size = cq.Vector(
    main_pcb.x
    + main_pcb_right_offset
    + max(pms5003.x, esp32_pcb_gap + esp32_pcb.x)
    + (lid_post_width * 2),
    ylen,
    70,
)

oled_pcb = cq.Vector(38, 12.4, 16)
oled_left_offset = 1
oled_right_offset = 6
oled_pcb_right_offset = 36
oled_pcb_top_offset = 31


main_pcb_hole_size = "M2"
main_pcb_post = HeatsertPost(
    7,
    internal_size.z - (oled_pcb.z + main_pcb.z),
    "M2",
)  # FIXME must be 17mm from lid
main_pcb_post.clearance = 5


main_pcb_lr_holes = 36.3 + main_pcb_post.bolt_diam()
main_pcb_tb_holes = 44.12 + main_pcb_post.bolt_diam()
main_pcb_left_offset = main_pcb_right_offset + main_pcb.x

esp32_post.clearance = 6

esp32_pcb_lr_holes = 24.1 + esp32_post.bolt_diam()

photo_sensor_hole_diameter = 4
photo_sensor_hole_pcb_top_offset = 14
photo_sensor_hole_pcb_left_offset = 7

bme680_vent_height = 15


power_pcb = cq.Vector(12.8, 13, 4)
power_pcb_post = PinPost(5, main_pcb_posts_padding + (power_pcb.z / 2), 3, power_pcb.z)
power_pcb_lr_holes = 5.85 + power_pcb_post.pin_width

lid_post = HeatsertPost(lid_post_width, internal_size.z, "M2")
lid_post.clearance = 10


class LayoutSolid(Base):
    def __init__(self):
        super().__init__()
        self.points = []
        self.solid = None
        self.shape = None

    def make(self):
        super().make()

        def place(loc):
            # return cq.Workplane("XY").add(self.solid).val().located(loc)
            return self.solid.located(loc)

        pts = self._get_points()
        self.shape = pts.eachpoint(place, combine=True)
        curr = self.shape.val().BoundingBox().center
        print(f"center at {curr}")

        self.shape = self.shape.translate(-curr)
        print(
            f"placed at {pts.vals()}, box: {self.shape.val().BoundingBox()}, sz: {self.size}"
        )

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = wp.add(self.shape)
        return self.shape.translate(self.origin_offset)

    def _get_points(self) -> cq.Workplane:
        return cq.Workplane("XY").pushPoints([(x.x, x.y) for x in self.points])


class Box(Base):
    def __init__(self):
        super().__init__()

    def make(self):
        super().make()

        self.shape = cq.Workplane("XY").box(self.size.x, self.size.y, self.size.z)

    def build(self) -> cq.Workplane:
        wp = super().build()
        shape = wp.add(self.shape.val())
        return shape.translate(self.origin_offset)


class MainShell(BaseShell):
    def __init__(self):
        super().__init__()
        self.from_internal_size(internal_size, wall_thickness)
        self.radius = floor(self.wall_thickness / 2)
        self.smooth = True
        self.pms5003_vent = None
        self.pms5003_right_support = None
        self.pms5003_top_support = None
        self.pms5003_bottom_support = None
        self.bme680_vent = None
        self.photo_sensor_hole = None
        self.pcb_posts = None
        self.esp32_posts = None
        self.power_pbc = None
        self.power_jack_hole = None
        self.lid_posts = None
        self.screw_holes = None
        self.bottom_support = None
        self.sides_debug = []
        self.debug_list = []

    def make(self):
        super().make()

        self.make_pms5003_vent()
        self.make_pms5003_support()
        self.make_bme680_vent()
        self.make_lcd_hole()
        self.make_photo_sensor_hole()
        self.make_pcb_posts()
        self.make_esp32_posts()
        self.make_power_pcb_posts()
        self.make_lid_posts()
        self.make_bottom_support()

        self.sides_debug = [
            place_on_face(
                cq.Workplane("XY").text("bottom", 12, 5).val(),
                self.outside_workplane("bottom"),
            ),
            place_on_face(
                cq.Workplane("XY").text("right", 12, 5).val(),
                self.outside_workplane("right"),
            ),
            place_on_face(
                cq.Workplane("XY").text("top", 12, 5).val(),
                self.outside_workplane("top"),
            ),
            place_on_face(
                cq.Workplane("XY").text("left", 12, 5).val(),
                self.outside_workplane(
                    "left",
                ),
            ),
            place_on_face(
                cq.Workplane("XY").text("front", 12, 5).val(),
                self.outside_workplane(
                    "front",
                ),
            ),
            place_on_face(
                cq.Workplane("XY").text("back", 12, 5).val(),
                self.outside_workplane(
                    "back",
                ),
            ),
        ]

    def make_lid_posts(self):
        self.lid_posts = PostSet()
        self.lid_posts.set_post_area(
            lid_post,
            self.size.x - ((self.wall_thickness * 2) + lid_post.width),
            self.size.y - ((self.wall_thickness * 2) + lid_post.width),
        )

        zoffset = self.lid_posts.size.z / 2
        self.lid_posts.origin_offset = cq.Vector(
            0,
            0,
            -zoffset - (self.wall_thickness / 2),
        )
        self.lid_posts.make()

        self.screw_holes = LayoutSolid()
        self.screw_holes.size = cq.Vector(
            self.lid_posts.size.x,
            self.lid_posts.size.y,
            self.wall_thickness,
        )

        def cborSolid():
            boreDir = cq.Vector(0, 0, -1)
            center = cq.Vector()
            # first make the hole
            hole = cq.Solid.makeCylinder(
                lid_post.bolt_diam() / 2.0, self.wall_thickness, center, boreDir
            )  # local coordinates!

            # add the counter bore
            cbore = cq.Solid.makeCylinder(
                lid_post.width / 2.0,
                self.wall_thickness / 2,
                cq.Vector(),
                boreDir,
            )
            return hole.fuse(cbore)

        self.screw_holes.solid = cborSolid()
        self.screw_holes.points = self.lid_posts.points

        self.screw_holes.origin_offset = cq.Vector(
            0,
            0,
            0,
        )
        self.screw_holes.make()

    def make_power_pcb_posts(self):
        self.power_pbc = PostSet()
        self.power_pbc.set_post_area(power_pcb_post, 0, power_pcb_lr_holes)
        self.power_pbc.padding = power_pcb_post.height / 2
        face = self.face_rect("front")
        posts_face = self.power_pbc.face_rect("front")
        xoffset = posts_face.align_x_offset(face, "right")
        yoffset = posts_face.align_y_offset(face, "bottom")

        # face = self.face_rect("left")
        # posts_face = self.power_pbc.face_rect("left")
        # zoffset = posts_face.align_y_offset(face, "bottom")
        zoffset = self.power_pbc.size.z / 2
        # zoffset = 0
        self.power_pbc.origin_offset = cq.Vector(
            xoffset - (self.wall_thickness + (self.power_pbc.size.y / 2)),
            yoffset
            + (
                self.wall_thickness
                + lid_post_width
                + 17  # fixme get a better offset here
            ),
            zoffset,
        )
        self.power_pbc.make()

        self.power_jack_hole = BoxStencil()
        self.power_jack_hole.size = cq.Vector(12, 8, self.wall_thickness)
        face = self.face_rect("bottom")
        jack_face = self.power_jack_hole.face_rect("front")
        xoffset = jack_face.align_x_offset(face, "left")
        yoffset = jack_face.align_y_offset(face, "bottom")
        self.power_jack_hole.origin_offset = cq.Vector(
            self.power_pbc.origin_offset.y,
            -(yoffset + (self.wall_thickness + (self.power_pbc.size.z / 2))),
            0,
        )
        self.power_jack_hole.make()

    def make_esp32_posts(self):
        self.esp32_posts = PostSet()
        self.esp32_posts.set_post_area(esp32_post, esp32_pcb_lr_holes, 0)
        self.esp32_posts.padding = pms5003.z

        face = self.face_rect("front")
        posts_face = self.esp32_posts.face_rect("front")
        xoffset = posts_face.align_x_offset(face, "right")
        yoffset = posts_face.align_y_offset(face, "top")

        face = self.face_rect("bottom")
        posts_face = self.esp32_posts.face_rect("bottom")
        # zoffset = posts_face.align_y_offset(face, "top")
        zoffset = self.esp32_posts.size.z / 2
        # zoffset = 0

        self.esp32_posts.origin_offset = cq.Vector(
            xoffset - (main_pcb_left_offset + esp32_pcb_gap + self.wall_thickness),
            yoffset - (main_pcb_top_offset + self.wall_thickness),
            zoffset,
        )
        self.esp32_posts.make()

    def make_pcb_posts(self):
        self.pcb_posts = PostSet()
        self.pcb_posts.set_post_area(
            main_pcb_post, main_pcb_lr_holes, main_pcb_tb_holes
        )
        self.pcb_posts.padding = 0

        face = self.face_rect("front")
        posts_face = self.pcb_posts.face_rect("front")
        xoffset = posts_face.align_x_offset(face, "right")
        yoffset = posts_face.align_y_offset(face, "top")
        zoffset = self.pcb_posts.size.z / 2

        self.pcb_posts.origin_offset = cq.Vector(
            xoffset - (main_pcb_right_offset + self.wall_thickness),
            yoffset - (main_pcb_top_offset + self.wall_thickness),
            zoffset,
        )
        self.pcb_posts.make()

    def make_bottom_support(self):
        self.bottom_support = Box()
        self.bottom_support.size = cq.Vector(
            self.pcb_posts.size.x + main_pcb_right_offset,
            self.size.y - self.pcb_posts.size.y - (self.wall_thickness * 2),
            self.pcb_posts.size.z,
        )
        face = self.face_rect("front")
        support_face = self.bottom_support.face_rect("front")
        xoffset = support_face.align_x_offset(face, "right")
        yoffset = support_face.align_y_offset(face, "bottom")
        zoffset = self.bottom_support.size.z / 2

        self.bottom_support.origin_offset = cq.Vector(
            xoffset - (self.wall_thickness),
            yoffset + (self.wall_thickness),
            zoffset,
        )
        self.bottom_support.make()

    def make_photo_sensor_hole(self):
        self.photo_sensor_hole = CylinderStencil()
        self.photo_sensor_hole.size = cq.Vector(
            photo_sensor_hole_diameter, photo_sensor_hole_diameter, self.wall_thickness
        )
        face = self.face_rect("front")
        hole_face = self.photo_sensor_hole.face_rect("front")
        xoffset = hole_face.align_x_offset(face, "right")
        yoffset = hole_face.align_y_offset(face, "top")
        self.photo_sensor_hole.origin_offset = cq.Vector(
            xoffset
            - (
                self.wall_thickness
                + main_pcb_right_offset
                + main_pcb.x
                + photo_sensor_hole_pcb_left_offset
            ),
            yoffset
            - (
                self.wall_thickness
                + main_pcb_top_offset
                + photo_sensor_hole_pcb_top_offset
            ),
            0,
        )
        self.photo_sensor_hole.make()

    def make_lcd_hole(self):
        self.lcd_hole = LCDStencil()
        self.lcd_hole.size = oled_pcb
        self.lcd_hole.size.x = self.lcd_hole.size.x - (
            oled_left_offset + oled_right_offset - oled_left_offset
        )
        self.lcd_hole.size.z = self.wall_thickness
        self.lcd_hole.radius = 1
        face = self.face_rect("front")
        lcd_hole_face = self.lcd_hole.face_rect("front")
        xoffset = lcd_hole_face.align_x_offset(face, "right")
        yoffset = lcd_hole_face.align_y_offset(face, "top")
        self.lcd_hole.origin_offset = cq.Vector(
            xoffset
            - (
                self.wall_thickness
                + main_pcb_right_offset
                + oled_pcb_right_offset
                + oled_right_offset
            ),
            yoffset - (self.wall_thickness + main_pcb_top_offset + oled_pcb_top_offset),
            0,
        )
        self.lcd_hole.make()

    def make_bme680_vent(self):
        self.bme680_vent = VentStencil()
        self.bme680_vent.size = cq.Vector(main_pcb.y / 2, 20, self.wall_thickness)
        self.bme680_vent.cols_from_width(self.wall_thickness / 2)
        self.bme680_vent.rows = 1
        self.bme680_vent.spacing = 1.5

        face = self.face_rect("right")
        vent_face = self.bme680_vent.face_rect("front")
        xoffset = vent_face.align_x_offset(face, "left")
        yoffset = vent_face.align_y_offset(face, "top")

        self.bme680_vent.origin_offset = cq.Vector(
            xoffset + 15 + self.wall_thickness,
            yoffset - self.wall_thickness - lid_depth,
            0,
        )
        self.bme680_vent.make()

    def make_pms5003_vent(self):
        self.pms5003_vent = VentStencil()
        self.pms5003_vent.size = cq.Vector(pms5003.y, pms5003.z, self.wall_thickness)
        self.pms5003_vent.cols = 12
        self.pms5003_vent.rows = 1
        self.pms5003_vent.spacing = 1.5

        face = self.face_rect("left")
        vent_face = self.pms5003_vent.face_rect("front")
        xoffset = vent_face.align_x_offset(face, "left")
        yoffset = vent_face.align_y_offset(face, "bottom")
        self.pms5003_vent.origin_offset = cq.Vector(
            xoffset
            + (self.wall_thickness + esp32_pms5003_y_gap + pms5003_support_width),
            yoffset + self.wall_thickness,
            0,
        )
        self.pms5003_vent.make()

    def make_pms5003_support(self):
        pms5003_support_height = min(4, pms5003_support_width)
        self.pms5003_right_support = Box()
        self.pms5003_right_support.size = cq.Vector(
            pms5003_support_width,
            self.size.y - self.wall_thickness,
            pms5003_support_height,
        )
        face = self.face_rect("front")
        support_face = self.pms5003_right_support.face_rect("front")
        xoffset = support_face.align_x_offset(face, "left")
        yoffset = support_face.align_y_offset(face, "top")
        zoffset = self.pms5003_right_support.size.z / 2

        self.pms5003_right_support.origin_offset = cq.Vector(
            xoffset + (self.wall_thickness + pms5003.x),
            # xoffset + self.wall_thickness,
            yoffset - (self.wall_thickness / 2),
            # 0,
            zoffset + (self.wall_thickness / 2),
        )
        self.pms5003_right_support.make()

        self.pms5003_top_support = Box()
        self.pms5003_top_support.size = cq.Vector(
            pms5003.x, pms5003_support_width, pms5003.z
        )
        support_face = self.pms5003_top_support.face_rect("front")
        xoffset = support_face.align_x_offset(face, "left")
        yoffset = support_face.align_y_offset(face, "top")
        zoffset = self.pms5003_top_support.size.z / 2
        self.pms5003_top_support.origin_offset = cq.Vector(
            xoffset + self.wall_thickness,
            yoffset - (self.wall_thickness + esp32_pms5003_y_gap),
            zoffset + (self.wall_thickness / 2),
        )
        self.pms5003_top_support.make()

        self.pms5003_bottom_support = Box()
        self.pms5003_bottom_support.size = cq.Vector(
            pms5003.x, lid_post_width, pms5003.z
        )
        support_face = self.pms5003_bottom_support.face_rect("front")
        xoffset = support_face.align_x_offset(face, "left")
        yoffset = support_face.align_y_offset(face, "top")
        zoffset = self.pms5003_bottom_support.size.z / 2

        self.pms5003_bottom_support.origin_offset = cq.Vector(
            xoffset + self.wall_thickness,
            yoffset
            - (
                self.wall_thickness
                + esp32_pms5003_y_gap
                + pms5003.y
                + pms5003_support_width
            ),
            zoffset + (self.wall_thickness / 2),
        )
        self.pms5003_bottom_support.make()

    def build(self) -> cq.Workplane:
        wp = super().build()

        part = place_on_face(
            self.pms5003_vent.build().val(),
            self.outside_workplane("left", offset=-self.wall_thickness / 2),
        )
        self.debug_list.append(part)
        shape = wp.cut(part)

        part = place_on_face(
            self.pms5003_bottom_support.build().val(),
            self.outside_workplane("back", invert=True, offset=self.wall_thickness / 2),
        )
        self.debug_list.append(part)
        shape = shape.union(part)

        part = place_on_face(
            self.pms5003_top_support.build().val(),
            self.outside_workplane("back", invert=True, offset=self.wall_thickness / 2),
        )
        self.debug_list.append(part)
        shape = shape.union(part)

        part = place_on_face(
            self.pms5003_right_support.build().val(),
            self.outside_workplane("back", invert=True, offset=self.wall_thickness / 2),
        )
        self.debug_list.append(part)
        shape = shape.union(part)

        part = place_on_face(
            self.bme680_vent.build().val(),
            self.outside_workplane("right", offset=-self.wall_thickness / 2),
        )
        self.debug_list.append(part)
        shape = shape.cut(part)

        part = place_on_face(
            self.photo_sensor_hole.build().val(),
            self.outside_workplane("front", offset=-self.wall_thickness / 2),
        )
        self.debug_list.append(part)
        shape = shape.cut(part)

        part = place_on_face(
            self.lcd_hole.build().val(),
            self.outside_workplane("front", offset=-self.wall_thickness / 2),
        )
        self.debug_list.append(part)
        shape = shape.cut(part)

        part = place_on_face(
            self.pcb_posts.build().val(),
            self.outside_workplane("back", invert=True, offset=self.wall_thickness),
        )
        self.debug_list.append(part)
        shape = shape.union(part)

        part = place_on_face(
            self.bottom_support.build().val(),
            self.outside_workplane("back", invert=True, offset=self.wall_thickness),
        )
        self.debug_list.append(part)
        shape = shape.union(part)

        part = place_on_face(
            self.esp32_posts.build().val(),
            self.outside_workplane("back", invert=True, offset=self.wall_thickness),
        )
        self.debug_list.append(part)
        shape = shape.union(part)

        part = place_on_face(
            self.power_pbc.build().val(),
            self.outside_workplane("back", invert=True, offset=self.wall_thickness),
        )
        self.debug_list.append(part)
        shape = shape.union(part)

        part = place_on_face(
            self.power_jack_hole.build().val(),
            self.outside_workplane(
                "right", invert=True, offset=self.wall_thickness / 2
            ),
        )
        self.debug_list.append(part)
        shape = shape.cut(part)

        part = place_on_face(
            self.lid_posts.build().val(),
            self.outside_workplane("front", offset=-self.wall_thickness / 2),
        )
        self.debug_list.append(part)
        shape = shape.union(part)

        part = place_on_face(
            self.screw_holes.build().val(),
            self.outside_workplane("front", offset=-self.wall_thickness / 2),
        )
        self.debug_list.append(part)
        shape = shape.cut(part)

        sides = cq.Workplane("XY")
        for s in self.sides_debug:
            sides = sides.union(s)
        self.debug_list.append(sides.val())

        return shape
        return shape.translate(self.origin_offset)


shell = MainShell()

shell.make()

model = shell.build()

model_post = (
    model.faces("|Y")
    .edges("|Z")
    .edges(
        selectors.BoxSelector(
            (
                -15,
                -(shell.size.y / 2) + shell.wall_thickness + 1,
                -(shell.size.z / 2) + shell.wall_thickness + 1,
            ),
            (
                ((shell.size.x / 2) - shell.wall_thickness - 1),
                ((shell.size.y / 2) - shell.wall_thickness - 1),
                ((shell.size.z / 2) - shell.wall_thickness - 1),
            ),
        )
    )
).chamfer(1)

bottom = model_post.faces(">>Z").workplane(offset=-lid_depth).split(keepBottom=True)
lid = model_post.faces(">>Z").workplane(offset=-lid_depth).split(keepTop=True)


if __name__ == "__main__":
    show_object([bottom, lid])
    # show_object([cq.Workplane("XY").add(x) for x in shell.debug_list])
    show_object(
        model.faces("|Y")
        .edges("|Z")
        .edges(
            selectors.BoxSelector(
                (
                    -esp32_pcb_gap / 2,
                    -(shell.size.y / 2) + shell.wall_thickness + 1,
                    -(shell.size.z / 2) + shell.wall_thickness + 1,
                ),
                (
                    ((shell.size.x / 2) - shell.wall_thickness - 1),
                    ((shell.size.y / 2) - shell.wall_thickness - 1),
                    ((shell.size.z / 2) - shell.wall_thickness - 1),
                ),
            )
        )
    )

if __name__ == "__cqgi__":
    show_object(bottom)
    show_object(lid)
