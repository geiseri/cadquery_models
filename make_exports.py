from cadquery import exporters

from aqi_case import bottom as aqi_case_bottom
from aqi_case import lid as aqi_case_lid
from top_groove import plineSweep as top_groove
from salt_level_meter import bottom_part_final as salt_level_meter_bottom
from salt_level_meter import top_part_final as salt_level_meter_top

exporters.export(aqi_case_bottom, "_static/aqi_case_bottom.3mf")
exporters.export(aqi_case_lid, "_static/aqi_case_lid.3mf")
exporters.export(salt_level_meter_bottom, "_static/salt_level_meter_bottom.3mf")
exporters.export(salt_level_meter_top, "_static/salt_level_meter_top.3mf")
exporters.export(top_groove, "_static/top_groove.3mf")
